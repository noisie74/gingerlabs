package com.example.gingerlabs.util;

import com.example.gingerlabs.model.GiphyData;

import java.util.List;

public interface LocalCache {

    <T> void writeToLocalCache(List<T> data, String query);
    List<GiphyData> readFromLocalCache(String queryKey);
}
