package com.example.gingerlabs.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.gingerlabs.model.GiphyData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Use Shared Preferences API as a local cache
 */
public class CacheUtil implements LocalCache {

    private Context context;

    public CacheUtil(Context context) {
        this.context = context;
    }

    @Override
    public <T> void writeToLocalCache(List<T> data, String queryKey) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(data);
        editor.putString(queryKey, json);
        editor.apply();
    }

    @Override
    public List<GiphyData> readFromLocalCache(String queryKey) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(queryKey, null);
        Type type = new TypeToken<List<GiphyData>>() {
        }.getType();
        return gson.fromJson(json, type); //Deserialize JSON object via GSON API
    }
}
