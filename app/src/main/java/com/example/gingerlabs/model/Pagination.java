package com.example.gingerlabs.model;

import com.google.gson.annotations.SerializedName;

public class Pagination {

    @SerializedName("total_count") private int totalCount;
    @SerializedName("count") private int count;
    @SerializedName("offset") private int offset;

    public Pagination(int totalCount, int count, int offset) {
        this.totalCount = totalCount;
        this.count = count;
        this.offset = offset;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
