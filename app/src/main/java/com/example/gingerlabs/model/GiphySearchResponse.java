package com.example.gingerlabs.model;

import java.util.List;

public class GiphySearchResponse {

    private final List<GiphyData> data;
    private final Meta meta;
    private final Pagination pagination;

    public GiphySearchResponse(List<GiphyData> data, Meta meta, Pagination pagination) {
        this.data = data;
        this.meta = meta;
        this.pagination = pagination;
    }

    public List<GiphyData> getData() {
        return data;
    }

    public Meta getMeta() {
        return meta;
    }

    public Pagination getPagination() {
        return pagination;
    }
}
