package com.example.gingerlabs.model;

public class Meta {

    private int status;

    public Meta(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
