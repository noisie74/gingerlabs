package com.example.gingerlabs.model;

public class GiphyData {

    private String id;
    private ImageFormats images;

    public GiphyData(String id, ImageFormats images) {
        this.id = id;
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ImageFormats getImages() {
        return images;
    }

    public void setImages(ImageFormats images) {
        this.images = images;
    }
}
