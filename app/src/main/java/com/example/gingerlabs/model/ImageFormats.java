package com.example.gingerlabs.model;

import com.google.gson.annotations.SerializedName;

public class ImageFormats {

    @SerializedName("fixed_width_small") private GiphyImage fixedWidthSmall;
    @SerializedName("fixed_width") private GiphyImage fixedWidth;
    @SerializedName("fixed_width_still") private GiphyImage fixedWidthStill;

    public ImageFormats(GiphyImage fixedWidthSmall, GiphyImage fixedWidth, GiphyImage fixedWidthStill) {
        this.fixedWidthSmall = fixedWidthSmall;
        this.fixedWidth = fixedWidth;
        this.fixedWidthStill = fixedWidthStill;
    }

    public GiphyImage getFixedWidthSmall() {
        return fixedWidthSmall;
    }

    public void setFixedWidthSmall(GiphyImage fixedWidthSmall) {
        this.fixedWidthSmall = fixedWidthSmall;
    }

    public GiphyImage getFixedWidth() {
        return fixedWidth;
    }

    public void setFixedWidth(GiphyImage fixedWidth) {
        this.fixedWidth = fixedWidth;
    }

    public GiphyImage getFixedWidthStill() {
        return fixedWidthStill;
    }

    public void setFixedWidthStill(GiphyImage fixedWidthStill) {
        this.fixedWidthStill = fixedWidthStill;
    }
}
