package com.example.gingerlabs.model;

public class GiphyImage {

    private String url;
    private String webp;
    private int width;
    private int height;

    public GiphyImage(String url, String webp, int width, int height) {
        this.url = url;
        this.webp = webp;
        this.width = width;
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebp() {
        return webp;
    }

    public void setWebp(String webp) {
        this.webp = webp;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
