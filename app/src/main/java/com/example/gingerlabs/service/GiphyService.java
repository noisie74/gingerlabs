package com.example.gingerlabs.service;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.gingerlabs.util.Constants.BASE_URL;

public class GiphyService { // retrofit client for API call

    public static GiphyApi apiCall() {

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(GiphyApi.class);
    }
}
