package com.example.gingerlabs.service;

import com.example.gingerlabs.model.GiphySearchResponse;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface GiphyApi {
    @GET("search") Observable<Response<GiphySearchResponse>> getSearchResults(
            @Query("api_key") String apiKey,
            @Query("q") String query,
            @Query("limit") int limit);
}

