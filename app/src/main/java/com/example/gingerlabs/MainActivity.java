package com.example.gingerlabs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.example.gingerlabs.model.GiphyData;
import com.example.gingerlabs.ui.Contract;
import com.example.gingerlabs.ui.GiphyImageAdapter;
import com.example.gingerlabs.ui.Presenter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements Contract.ViewContract, View.OnClickListener {

    @BindView(R.id.toolbar) Toolbar mToolBar;
    @BindView(R.id.fab) FloatingActionButton mFabButton; //search button
    @BindView(R.id.search_et) EditText mSearchEditText;
    @BindView(R.id.progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;

    private Presenter mPresenter;
    private GiphyImageAdapter mImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);

        mPresenter = new Presenter(this);
        mImageAdapter = new GiphyImageAdapter(this, new ArrayList<GiphyData>());
        mRecyclerView.setAdapter(mImageAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFabButton.setOnClickListener(this);
    }

    @Override
    public String getSearchQuery() {
        return mSearchEditText.getText().toString();
    }

    @Override
    public void onClick(View v) {
        mPresenter.loadGIFs();
    }

    @Override
    public void showGIFs(List<GiphyData> giphyDataList) {
        mImageAdapter.setData(giphyDataList);
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void hideKeyBoard() { // hide keyboard when search button clicked
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public Context provideContext() {
        return getApplicationContext();
    }// provide context to presenter for shared preferences

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unSubscribe(); // avoid memory leaks
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mToolBar.setTitle(R.string.app_name);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_exit) {
            this.finish(); // exit the app
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
