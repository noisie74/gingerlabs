package com.example.gingerlabs.ui;

import android.util.Log;

import com.example.gingerlabs.model.GiphyData;
import com.example.gingerlabs.model.GiphySearchResponse;
import com.example.gingerlabs.service.GiphyService;
import com.example.gingerlabs.util.Constants;
import com.example.gingerlabs.util.CacheUtil;

import java.util.List;
import java.util.Objects;

import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class Presenter implements Contract.PresenterContract {

    private CompositeSubscription mSubscription;
    private Contract.ViewContract mView;

    public Presenter(Contract.ViewContract mView) {
        this.mView = mView;
        mSubscription = new CompositeSubscription();
    }

    @Override
    public void loadGIFs() {
        final String query = mView.getSearchQuery();
        final CacheUtil cache = new CacheUtil(mView.provideContext()); // context object required for shared prefs
        final List<GiphyData> cachedGiphyData = cache.readFromLocalCache(query); // First try to read data from cache
        mView.showProgressBar();
        mSubscription.add(GiphyService.apiCall().getSearchResults(Constants.API_KEY, query, 20)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Response<GiphySearchResponse>>() {
                    @Override
                    public void call(Response<GiphySearchResponse> giphySearchResponse) {
                        List<GiphyData> data = giphySearchResponse.body().getData();
                        cache.writeToLocalCache(data, query); //save to cache - shared preferences

                        if (cachedGiphyData != null) {
                            observeSearchResults(cachedGiphyData);
                        } else {
                            observeSearchResults(data);
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d("SERVICE ERROR!", Objects.requireNonNull(throwable.getMessage()));
                    }
                }));
    }

    private void observeSearchResults(List<GiphyData> giphyDataList) {
        mView.hideKeyBoard();
        mView.showGIFs(giphyDataList);
        mView.removeProgressBar();
    }

    @Override
    public void unSubscribe() {
        mSubscription.unsubscribe(); // clear Subscription to avoid memory leak
    }
}
