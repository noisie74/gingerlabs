package com.example.gingerlabs.ui;

import android.content.Context;
import com.example.gingerlabs.model.GiphyData;
import java.util.List;

public interface Contract {

    interface ViewContract {

        void showGIFs(List<GiphyData> giphyDataList);

        String getSearchQuery();

        Context provideContext();

        void showProgressBar();

        void removeProgressBar();

        void hideKeyBoard();
    }

    interface PresenterContract {

        void loadGIFs();

        void unSubscribe();
    }
}
