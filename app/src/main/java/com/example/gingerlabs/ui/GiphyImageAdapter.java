package com.example.gingerlabs.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.gingerlabs.R;
import com.example.gingerlabs.model.GiphyData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.core.util.Preconditions.checkNotNull;

public class GiphyImageAdapter extends RecyclerView.Adapter<GiphyImageAdapter.ViewHolder> {

    private List<GiphyData> dataList;
    private Context context;

    public GiphyImageAdapter(Context context, List<GiphyData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public void setData(List<GiphyData> dataList) {
        this.dataList = checkNotNull(dataList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.giphy_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String giphyImage = dataList.get(position).getImages().getFixedWidth().getUrl();
        Glide.with(context).load(giphyImage).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.gifImage); // cache images to better performance
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image) ImageView gifImage;

        public ViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
